import time
from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
from bs4 import BeautifulSoup as bs
from google_play_scraper import app
from selenium.webdriver.common.by import By
import re
import pandas as pd
import json
import requests
from googleAppScraper import PlayAppScraper


class GoogleScraper:
    def __init__(self):
        self.options = webdriver.ChromeOptions()
        self.driver = webdriver.Chrome(ChromeDriverManager().install())
        self.driver.maximize_window()
        self.links_list=[]
        self.results=[]
        
    def main(self,page):
        if page =="" :return []
        self.driver.get(page)
        time.sleep(10)
        SCROLL_PAUSE_TIME = 5
        # Get scroll height
        last_height = self.driver.execute_script("return document.body.scrollHeight")
        time.sleep(SCROLL_PAUSE_TIME)
        while True:
                # Scroll down to bottom
                self.driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
 
                # Wait to load page
                time.sleep(SCROLL_PAUSE_TIME)
 
                # Calculate new scroll height and compare with last scroll height
                new_height = self.driver.execute_script("return document.body.scrollHeight")
                if new_height == last_height:
                    break
                last_height = new_height
 
       
        elems = self.driver.find_elements(By.XPATH, "//a[@href]")  
        if elems == None: return []
        s=PlayAppScraper()
        for elem in elems:
            if "details?id" in elem.get_attribute("href"):
                if not (elem.get_attribute("href") in self.links_list):
                    self.links_list.append(elem.get_attribute("href"))
                    self.results.append(s.scrapApp(elem.get_attribute("href")))
                    time.sleep(3) 
        #print(len(self.links_list))

        self.driver.close()
        #we can save data at first in a json file
        #with open(page[page.find("q=")+2:-7]+"_AppleApps.json","w") as final:
         #            json.dump(self.results, final)
        return self.results

                
'''       
s=GoogleScraper()            
print(s.main("http://play.google.com/store/search?q=HEALTH_AND_FITNESS&c=apps"))
'''
