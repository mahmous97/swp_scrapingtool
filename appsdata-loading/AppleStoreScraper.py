import requests
from bs4 import BeautifulSoup as bs
import time
import json
from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
import pandas as pd
from selenium.webdriver.support.wait import WebDriverWait, TimeoutException
from selenium.webdriver.support import expected_conditions as EC
from selenium.common import exceptions
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.service import Service
from AppleAppScraper import Scraper, Helper



class AppleScraper:
  def __init__(self):
        self.list_of_pages_of_letters = []
        self.list_of_pages_numbers = []
        self.list_of_apps = []
        self.options = webdriver.ChromeOptions()
        self.driver = webdriver.Chrome(ChromeDriverManager().install())
        self.driver.maximize_window()
        
  def get_letters(self,url):

        self.driver.get(url)
        time.sleep(5)
        elems = self.driver.find_elements(By.XPATH, "//a[@href]")
        if elems == None :return []
        #global self.list_of_pages_of_letters
        for elem in elems:
            if ("letter="in elem.get_attribute("href")):
                if not (elem.get_attribute("href") in self.list_of_pages_of_letters):
                    self.list_of_pages_of_letters.append(elem.get_attribute("href"))
                    #get_nums_of_letter(elem.get_attribute("href"))
                    
        return self.list_of_pages_of_letters           
                    #print(elem.get_attribute("href"))
        #return(list_of_pages_of_letters)
        
        
        
        
        
        
  def get_nums_of_letter(self,letter):
#        print("find numbers of "+letter+"\n")

        self.driver.get(letter)

        time.sleep(5)
        elems = self.driver.find_elements(By.XPATH, "//a[@href]")
        if elems == None:return []
        #global self.list_of_pages_numbers
        for elem in elems:
            if "page=" in elem.get_attribute("href"):
                if not (elem.get_attribute("href") in self.list_of_pages_numbers):
                    self.list_of_pages_numbers.append(elem.get_attribute("href"))
                    #get_apps_num_letter(elem.get_attribute("href"))
        return(self.list_of_pages_numbers)
    
  def get_apps_num_letter(self,nr):
      #  print("find apps of "+nr+"\n")
        self.driver.get(nr)
        time.sleep(5)
        SCROLL_PAUSE_TIME = 5
        # Get scroll height
        last_height = self.driver.execute_script("return document.body.scrollHeight")
        time.sleep(SCROLL_PAUSE_TIME)
        while True:
                # Scroll down to bottom
                self.driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
 
                # Wait to load page
                time.sleep(SCROLL_PAUSE_TIME)
 
                # Calculate new scroll height and compare with last scroll height
                new_height = self.driver.execute_script("return document.body.scrollHeight")
                if new_height == last_height:
                    break
                last_height = new_height
        elems = self.driver.find_elements(By.XPATH, "//a[@href]")
        if elems == None: return []
       # global self.list_of_apps
        for elem in elems:
            if "https://apps.apple.com/de/app/"in elem.get_attribute("href"):
                if not (elem.get_attribute("href") in self.list_of_apps):
                    self.list_of_apps.append(Scraper().scrapApp(elem.get_attribute("href")))
        #print(len(list_of_apps))


        return self.list_of_apps
        
        
  def main(self,pageUrl):
   # print("Start!")
    letters = self.get_letters(pageUrl)
    pages=[]
    for letter in letters:#just for testing
        if len(pages)<=2:
            pages = pages + self.get_nums_of_letter(letter)
        else:break
    apps=[]
    for page in pages:
        if len(apps)<=50:#just for testing
            apps = apps + self.get_apps_num_letter(page)
        else:break
  #  print(len(pages))
  #  print(len(apps))  

    self.driver.close()
    
   # print("Done!")

    #with open(pageUrl[-14:-7]+"_AppleApps.json", "w") as final:
        #json.dump(apps, final)
     
    return apps
   # print("Thanks!")
'''  
apple = AppleScraper()
apple.main("https://apps.apple.com/de/genre/ios-medizin/id6020")
'''





            

